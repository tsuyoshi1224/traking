
let sucArray = [];
let faiArray = [];
let counter  = 0;//荷物検索残数
let count    = 0;//荷物残数
let Result   =[];//全県



let resultOut = function(arrays)
{
  try {
    let result = [];
    let css    = "";

    Result.push(JSON.parse(JSON.stringify(arrays)));

    if (arrays.code >= 400) {
      faiArray.push(arrays);
    }

    result.push(arrays.slipNo);

    if (arrays.code === 200) {

      if (arrays.companyName === "sagawa") {
        result.push('佐川急便');
        css = "success";
      }else if(arrays.companyName === "yamato"){
        result.push('ヤマト運輸');
        css = "info";
      }else if(arrays.companyName === "japanpost"){
        result.push('日本郵便');
        css = "warning";
      }

      result.push(arrays.statusList[arrays.statusList.length-1].date);
      result.push(arrays.statusList[arrays.statusList.length-1].time);
      result.push(arrays.statusList[arrays.statusList.length-1].placeName);
      result.push(arrays.statusList[arrays.statusList.length-1].placeCode);
      result.push(arrays.statusList[arrays.statusList.length-1].status);
      csvResult(ArrayToCSV(result));

      var body = [
        '<tr class="'+css+'">',
        '<td>'+result[1]+'</td>',
        '<td>'+result[0]+'</td>',
        '<td>'+result[6]+'</td>',
        '<td>'+result[2]+'</td>',
        '<td>'+result[3]+'</td>',
        '<td>'+result[4]+'</td>',
        "<td><a href='#' data-toggle='modal' data-target='#detailModal' onclick='detailModal("+arrays.slipNo+",&quot;"+arrays.companyName+"&quot;)'>詳細表示",
        '</td>'+'</tr>'
      ].join("");

    }
    $('#list-result tbody').append(body);
  } catch (e) {
    $('.modal-dialog').find('.modal-title').html('エラー!');
    $('.modal-dialog').find('.modal-body tbody').append(e);
  }
}

let failDataOut = function()
{
  var array = {};


  for (let i in faiArray) {
    if (array[faiArray[i]['slipNo']]) {
      array[faiArray[i]['slipNo']][i] = faiArray[i];
    }else{
      array[faiArray[i]['slipNo']] = [];
      array[faiArray[i]['slipNo']][i] = faiArray[i];
    }
  }

  if (count === 0) {
    for (let n in array) {
      let length = Object.keys(array[n]).length;

      if (length > 3 || length === 3) {

        let tdarray = array[n][array[n].length-1];
        var body = [
          '<tr class="warning">',
          '<td>-</td>',
          '<td>'+tdarray.slipNo+'</td>',
          '<td>'+tdarray.status+'</td>',
          '<td>-</td>',
          '<td>-</td>',
          '<td>-</td>',
          "<td>-",
          '</td>'+'</tr>'
        ].join("");
      }
      $('#list-result tbody').append(body);
    }
  }





}


let resultListCheck = function(obj)
{
  try {
    if (obj.length === 0) {
      throw new Error('データ取得エラー');
    }
    resultOut(obj);
  } catch (e) {
    alert(e+'2');
    $('.modal-dialog').find('.modal-title').html('エラー!');
    $('.modal-dialog').find('.modal-body tbody').append(e);
  }
}

let csvResult = function (text)
{
  let oldValues = document.getElementById('resultTextArea').value
  let textarea = document.getElementById('resultTextArea');
  if (oldValues == "") {
    textarea.value = text;
  }else{
    textarea.value = oldValues+"\n"+text;
  }
}


let TexttoArray =function(text) {
  arr = text.split(/\r\n|\r|\n/);
  counter = arr.length * 3;
  return arr;
}

let numberSearch = function()
{
  let text = $("#slipNo").val();
  let textArray = TexttoArray(text);
  var result = $.grep(textArray, function(e){return e !== "";});
  return result;
}

let ajaxSearch = function()
{
  try {
    Result   =[];
    sucArray = [];
    faiArray = [];
    counter  = 0;//荷物検索残数
    count    = 0;//荷物残数


    let companyName = ['japanpost','sagawa','yamato'];
    let number = numberSearch();

    for(let i in companyName)
    {
      for(let ii in number)
      {
        let url = 'traking/'+companyName[i]+'?number='+number[ii];
        ajax(url);
      }
    }

  } catch (e) {
    alert(e+'3');
    $('.modal-dialog').find('.modal-title').html('エラー!');
    $('.modal-dialog').find('.modal-body tbody').append(e);
  } finally {
    initText();
  }


}

let ArrayToCSV = function(array)
{
  let csv="";
  for(let i=0;i<array.length;i++){
    if (array[i] == null) {
      csv+=""+",";
    }else{
      csv+=array[i]+",";
    }

  }
  csv=csv.slice(0,-1);
  return csv;
}

let execCopy = function()
{
  let text = document.getElementById("resultTextArea");
  text.select();
  document.execCommand("copy");
}

let initText = function()
{
  document.getElementById("resultTextArea").value="";
  $('table tbody *').remove();
}
