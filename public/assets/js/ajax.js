function ajax(url)
{
  try {
    $.ajax({
      type: 'GET',
      url: url
    }).done(function(data){
      resultListCheck(data);
    }).fail(function(){
      throw new Error('ajax通信エラー');
    }).always(function(){
      counter --;
      count = counter / 3;
      failDataOut();
    });
  } catch (e) {
    alert(e);
    //モーダルでエラー表示
    $('.modal-dialog').find('.modal-title').html('エラー!');
    $('.modal-dialog').find('.modal-body tbody').append(e);
  }
}
