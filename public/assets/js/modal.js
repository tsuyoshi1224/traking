function detailModal(slipNo,companyName)
{
  //init
  $('.modal-dialog').find('.modal-title').html("");
  $('.modal-dialog').find('.modal-body tbody').html("");

  $('.modal-dialog').find('.modal-title').html('荷物番号：'+slipNo);

  var result = Result.filter(function(item, index){
    if (item.slipNo === slipNo && item.companyName === companyName) return true;
  });


  for(let i in result[0].statusList)
  {
    var body = [
      '<tr>',
      '<td>',
      result[0].statusList[i].status,
      '</td>',
      '<td>',
      result[0].statusList[i].date,
      '</td>',
      '<td>',
      result[0].statusList[i].time,
      '</td>',
      '<td>',
      result[0].statusList[i].placeName,
      '</td>',
      '<td>',
      result[0].statusList[i].placeCode,
      '</td>',
      '</tr>',

    ].join("");
    $('.modal-dialog').find('.modal-body tbody').append(body);
  }
}
