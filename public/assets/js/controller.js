
var titleCtrl = function($scope)
{
  $scope.title = "荷物トラッカー";
}

var mainCtrl = function($scope,$http,$q)
{

  $scope.onclick = function()
  {
    //検索モーダル
    myModalInstance.show();

    var csv = $scope.number;
    var obj = csv.split(',');
    //検索残り時間
    $scope.remaining = obj.length;
    $scope.counter = obj.length;

    var parameter = JSON.stringify(obj);

    $http({
      method: 'POST',
      url: "/traking/allsearch",
      data: parameter,
      timeout:300000
    })
    .success(function onSuccess(data, status, headers, config) {
      // 通信成功
      myModalInstance.hide();
      $scope.result = data;

      var japanpostArray = [];
      var otherArray = [];
      var resultArray = [];

      angular.forEach(data,function(value,key){
        if (value.statusList) {
          if(value.companyName === "japanpost"){
            var japanpost = new Object;
            japanpost.status    = value.statusList[value.statusList.length-1].status;
            japanpost.date      = value.statusList[value.statusList.length-1].date;
            japanpost.time      = value.statusList[value.statusList.length-1].time;
            japanpost.placeName = value.statusList[value.statusList.length-1].placeName;
            japanpost.placeCode = value.statusList[value.statusList.length-1].placeCode;

            japanpostArray.push(value.slipNo);
            japanpostArray.push(value.companyName);
            angular.forEach(japanpost,function(value,key){
              japanpostArray.push(value);
              otherArray.push("\n");
            });
            var csv = ArrayToCSV(japanpostArray);
            resultArray.push(csv.replace(/null/g , "-"));
          }else{

            otherArray = [];
            otherArray.push(value.slipNo);
            otherArray.push(value.companyName);
            angular.forEach(value.statusList[value.statusList.length-1],function(value,key){
              otherArray.push(value);
            });
            resultArray.push(ArrayToCSV(otherArray));
          }
        }else{

          otherArray = [];
          otherArray.push(value.slipNo);
          otherArray.push(value.companyName);
          angular.forEach(value,function(values,key){
            otherArray.push(values);
            otherArray.push("\n");
          });
          ArrayToCSV(otherArray);
        }
        $scope.csvs = resultArray;

      });
    })
    .error(function onError(data, status, headers, config) {
      // 通信失敗
      var title = "通信エラー";
      var content = "通信エラーが発生しました　更新して再度実行してください。";
      $scope.modalTitle = title;
      $scope.errorContent = content;
      searchModalInstance.show();
    });
  };

  $scope.modal = function(status)
  {
    var title = '荷物番号';
    $scope.modalTitle = title+status.slipNo;
    $scope.modalContent = status;
    console.log(status);
    searchModalInstance.show();
  };

  function ArrayToCSV(array){
    var csv="";
    for(var i=0;i<array.length;i++){
      csv+=array[i]+",";
    }
    csv=csv.slice(0,-1);
    return csv;
  }

};
