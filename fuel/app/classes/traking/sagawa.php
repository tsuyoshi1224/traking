<?php

class Traking_Sagawa
{
  private $placeName;
  private $number;
  public $contents;
  public $placeCodeArray;
  public $status;
  public $detail;
  public $keys;

  public function __construct()
  {
    $url = Uri::base().'/placecode/sagawa';
    $this->placeCodeArray = json_decode(file_get_contents($url, false),true);
    $this->keys = array("space","status","date","time","placeName","placeCode");
  }
  //営業所コード取得
  public function sagawaPlaceCodeSearch($placeName)
  {
    if (array_key_exists($placeName,$this->placeCodeArray)) {
      $placeCode = $this->placeCodeArray[$placeName];
    }else{
      $placeCode = "ー";
    }
    return $placeCode;
  }


  //ステータス配列の作成
  public function statusGet($number)
  {
    $this->number = $number;

    $vali = new Traking_Common($number);
    $vali -> validation();

    //チェックディジット
    $traking_number = Traking_Common::checkdigit($this->number);
    $this->number = $traking_number['traking_number'];

    // HTMLファイルの取得
    $html = Traking_Common::fileget($this->number);
    mb_convert_variables('UTF-8' , 'HTML-ENTITIES' , $html);
    $contents = preg_replace('/(\t|\r\n|\r|\n)/s', '', $html);
    // preg_match_all("/<t.[^>]*>([^<]*)<\/t.>/i", $contents, $contents);

    //HTMLからtdタグのみを取り出しタグを削除
    preg_match_all('/<td.*?>(.*?)<\/td>/iu', $contents, $contents);
    foreach ($contents[0] as $key => $value) {
      $notag[] = strip_tags($value);
      $notags[] = preg_replace( "/^\xC2\xA0/", "", $notag[$key] );
    }
    // タイトル部分と詳細部分の分離
    $title = array_slice($notags,0,5);
    $top_detail = array_slice($notags,6,4);
    $content_detail = array_slice($notags,10);

    $title_key =        array('title_number','title_status','title_statsus','title_date','title_redelivery');
    $title_array = array_combine($title_key,$title);


    //荷物データの有無
    if (count($contents[0]) == 16) {
      throw new Exception('number_error');
    }
    if (count($content_detail) === 1) {
      //てスト
      // $content_detail = array('⇒配達完了','―','八千代営業所','');
      //荷物未登録
      throw new Exception('unregistered');//商品が取得出来なかった場合
    }

    $this->contents = $content_detail;
  }


  public function dateBuilding()
  {
    $key_date_time = array('date','time');
    $n=1;
    foreach ($this->contents as $key => $value) {
      if (count($this->contents)<$n) {
        break;
      }else{
        $date_time[] = current(array_slice($this->contents, $n, 1, true));
      }
      $n +=3;
    }


    foreach ($date_time as $key => $value) {
      $date[] = preg_split("/[\s,]+/", $value);
      //日付データがない場合
      if (!empty($date_time[0]) && count($date_time) === 1) {
        $date[0][1] = '―';
      }
    }


    return $date;
  }


  public function detailBulding()
  {
    $contents = $this->contents;

    $n=1;
    foreach ($contents as $key => $value) {
      $detail_array = array_splice($contents,$n,1);
      $n+=2;
    }
    array_slice($this->contents, $n,1,true);
    $contents = array_chunk($contents,2);

    return $contents;
  }



  public function statusBuilding()
  {

    $key_date_time = array('date','time');
    $split_date_time = $this->dateBuilding();
    $detail_array = $this->detailBulding();


    foreach ($detail_array as $key => $value) {
      $placeCode[][] = $this->sagawaPlaceCodeSearch($value[1]);
    }

    $this->detail = array();
    foreach ($placeCode as $key => $value) {
      $this->detail[] = "";
      $this->detail[] = $detail_array[$key][0];

      foreach ($split_date_time[0] as $keyDate => $valueDate) {
        $this->detail[] = $valueDate;
      }

      $this->detail[] = $detail_array[$key][1];
      $this->detail[] = $placeCode[$key][0];
    }

    if((int)count($this->detail) % 6 === 0) {
      $baggage = array_chunk($this->detail,6);

      foreach($baggage as $key => $value){
        $status_array = array_combine($this->keys, $baggage[$key]);
        unset($status_array['space']);
        $this->status[] = $status_array;
      }
    }else{
      throw new Exception('count_error');
    }
    return $this->status;
  }

}
