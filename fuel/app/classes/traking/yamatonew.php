<?php
class Traking_Yamatonew
{
  private $number;
  private $node;
  public $results;

  public function __construct($number,$html)
  {
    $this->number = $number;
    $this->html   = $html;
    $this->history_status        = '/html/body/center[3]/table[3]';
    $this->bag_discriminant      = '/html/body/center[3]/table[2]';
    $this->bag_discriminant_flag = 0; //0通常荷物 1DM


    $vali = new Traking_Common($this->number);
    $vali -> validation();

    $traking_number = Traking_Common::checkdigit($this->number);
    $this->number = $traking_number['traking_number'];

    return $this->number;
  }

  public function tracking_history()
  {
    $dom = $this-> html_to_dom();
    $xml = $this->get_xml();
    $xpath = new DOMXpath($dom);

    //商品名検索
    $this->bag_discriminant();

    //商品が取得判別処理
    if ($xpath->query($this->get_history_status())->length === 0) {
      throw new Exception('unregistered');//商品が検索できなかった場合
    }

    foreach ($xpath->query($this->get_history_status()) as $node) {
      $this->node = $node;
      break;
    }

    $node_array = $this->nodeObject_Array();
    $contents = array_slice($node_array,7);

    //文字列内部の空白削除
    foreach ($contents as $key => $value) {
      $content[] = trim($value);
    }

    //DMの場合の処理
    if (empty($content[0]) && $this->bag_discriminant_flag === 1) {
      $content = array_values($content);

      foreach ($content as $key => $value) {
        if ($key % 7 === 0) {
          unset($content[$key]);
        }
      }
    }

    $this->content = $content;
    $this->status_build();
  }

  private function status_build()
  {
    $bags=(array_chunk($this->content,6,true));//配列を６つに分割
    $keys =array(
      "status",
      "date",
      "time",
      "placeName",
      "placeCode",
      "space",
    );
    foreach ($bags as $key => $value) {
      $result[] = array_combine($keys,$value);
    }
    foreach ($result as $key => $value) {
      unset($result[$key]['space']);
    }

    $this->results = $result;
  }

  private function bag_discriminant()
  {
    $dom = $this->html_to_dom();
    $xpath = new DOMXpath($dom);

    //クロネコDM便対策
    if ($xpath->query($this->get_bag_discriminant())->length === 0) {
      throw new Exception('bag_unregistered');//商品が取得出来なかった場合
    }

    foreach ($xpath->query($this->get_bag_discriminant()) as $title_node) {
      $this->node = $title_node;
      break;
    }
    $title_node = $this->nodeObject_Array();

    //商品名判別
    if (count($title_node) <= 3) {
      Log::debug('DM品:'.$this->number);
      $this->bag_discriminant_flag = 1;//フラグを立てる
    }

  }

  private function get_bag_discriminant(){
    return $this->bag_discriminant;
  }

  private function nodeObject_Array()
  {
    $node = $this->get_node();
    $node_array = explode("\n", $node->textContent);
    return $node_array;
  }


  private function get_node()
  {
    return $this->node;
  }

  private function html_to_dom()
  {
    $dom = new DOMDocument();
    @$dom->loadHTML($this->get_html());
    $this->xml = simplexml_import_dom($dom);
    return $dom;
  }

  private function get_html()
  {
    return $this->html;
  }

  private function get_history_status()
  {
    return $this->history_status;
  }
  private function get_xml()
  {
    return $this->xml;
  }

  public function get_result()
  {
    return $this->results;
  }

}
