<?php
class Traking_Validation
{
  public $number;

  public function __construct($number)
  {
    $this->number = $number;
    
    if (mb_strlen($this->number) === 0) {
      throw new Exception('unentered');//番号未入力
    }
    if (mb_strlen($this->number) != 12) {
      throw new Exception('number_error');//12ではなかった場合
    }
    return $this->number;

  }
}
