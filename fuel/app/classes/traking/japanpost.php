<?php

class Traking_Japanpost
{
  private $number;
  public $html;
  public $contents;
  public $result;

  public function __construct($number,$html)
  {
    $this->html = $html;
    $this->number = $number;
    mb_language('Japanese');//エンコードエラー対策
    // $this -> contents = mb_convert_encoding($this->html,'utf8','SJIS');
    $this -> contents = str_replace(array("\r", "\n"), '', $this -> html);

    $vali = new Traking_Common($this->number);
    $vali -> validation();

    //チェックディジット
    $traking_number = Traking_Common::checkdigit($this->number);
    $this->number = $traking_number['traking_number'];

    return $this->number;

  }
  public function traking_history()
  {
    preg_match('/<table.class="tableType01.txt_c.m_b5".summary="履歴情報">(.*?)<\/table>/',$this->contents,$result);

    if (empty($result)) {
      throw new Exception('unregistered');//商品が取得出来なかった場合
    }

    //HTMLタグを基準に配列へ
    preg_match_all('/(<[^>]+>[^<]+<\/[^>]+>|[^<]+)/', $result[1], $detail);

    $baggage_datas=preg_replace('/<("[^"]*"|\'[^\']*\'|[^\'">])*>/','',$detail[1]);
    //残ったtdタグの削除
    $bag = (preg_replace('/.*>/',' ',$baggage_datas));

    //配列に残った空白の削除
    for($i=0;$i<count($bag);$i++){
      $bags[] = preg_replace("/( |　)/", "", $bag[$i]);
    }
    //必要最低限の要素を配列へ
    $result_bag = (array_values(array_filter($bags,'strlen')));
    $result_bag = (array_splice($result_bag, 6));



    //日付とデータを分ける
    for($i=0;$i<count($result_bag);$i++){
      if($i%5 == 1){//荷物状況の仕分け
        $status[] = array('status' => $result_bag[$i]);
      }


      if($i%5 == 0){//日付と時刻の仕分け

        // 時刻データがない場合
      if (strlen($result_bag[$i]) === 10) {
        $date[] = array('date' => substr($result_bag[$i], -5));
        $time[] = array('time' => "");
      }else{
        $date[] = array('date' => substr($result_bag[$i], -10, -5));
        $time[] = array('time' => substr($result_bag[$i], -5));
      }

      }
      if($i%5 == 2){//営業所店の仕分け

        //詳細情報が出ている場合の処理
       if (preg_match("/[0-9]/", $result_bag[$i])) {
         $remarks[] = $result_bag[$i];
        unset($result_bag[$i]);
        $result_bag = array_values($result_bag);
       }

        $placeName[] = array('placeName' => $result_bag[$i]);
      }
    }

    for($i=0;$i<count($status);$i++){
      $keys = array(
        "date" => $date[$i]['date'],
        "time" => $time[$i]['time'],
        "status" => $status[$i]['status'],
        "placeName" => $placeName[$i]['placeName'],
        "placeCode" => null,
      );
      $this->result[] = $keys;
    }
    print_r($this->result);
    exit;
    return $this->result;
  }

public function statusBuilding()
{
  return $this->result;
}

}
