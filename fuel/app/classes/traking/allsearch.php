<?php
class Traking_Allsearch
{
  public $raw_post;
  public $searchArray;
  public $result;
  public $company_name;
  public $url;



  public function __construct($raw_post)
  {
    $this->url = Uri::base(false);

    if ($raw_post) {
      $this->raw_post = $raw_post;
    }

    $this->company_name = array(
        'sagawa',
        'yamato',
        'japanpost',
        );
  }

  public function is_json($string){
    return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
  }

  public function jsonToArray($json)
  {
    if(!isset($json)){
      throw new Exception('{"error_code":404,"status":"No Post Data"}');//なんのPOSTも投げられなかった場合
    }else{
      if (!$this->is_json($json)) {
        throw new Exception('{"error_code":404,"status":"No Post JSON"}');//JSON型ではない場合
      }else{
        return json_decode($this->raw_post,true);
      }
    }
  }

  public function apiAcces($apiUrl)
  {
    try {
      $http_response_header = array();
      if($data = @file_get_contents($apiUrl)){
        return $data;
      }else{
        if(count($http_response_header) > 0){
          $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る
          switch($status_code[1]){
            case 404:
              throw new Exception('{"error_code":404,"status":"指定したページが見つかりませんでした"}');
              break;
            case 500:
              throw new Exception('{"error_code":500,"status":"指定したページがあるサーバーにエラーがあります"}');
              break;
            default:
              throw new Exception('{"error_code":501,"status":"何らかのエラーによって指定したページのデータを取得できませんでした"}');
          }
        }else{
          throw new Exception('{"error_code":502,"status":"何らかのエラーによって指定したページのデータを取得できませんでした"}');
        }
      }
    } catch (\Exception $e) {
      throw new Exception($e->getMessage());
    }
  }

  public function postToArray()
  {
    $this->searchArray = $this->jsonToArray($this->raw_post);
    foreach ($this->searchArray as $key => $searchNumber) {
      $searchNumber = preg_replace('/[^0-9\.]/', '', $searchNumber);
      foreach ($this->company_name as $ckey => $companyName) {
        $apiUrl = $this->url.'/traking/'.$companyName.'?number='.(int)$searchNumber;
        $response =  $this->apiAcces($apiUrl);
        $apiResultArray[] = json_decode($response);//Arrayだとうまく行かないのでオブジェクト型に変更;
      }
    }
    return $apiResultArray;
  }


  public function objToArray($obj)
  {
    $json = json_encode($obj);
    $array = json_decode($json,true);
 #   foreach ($obj as $key => $value) {
 #     $array[] = (array)$value;
 #   }
    return $array;
  }

  public function noOrderArray()
  {
    $response = $this->postToArray();
    $responseArray = $this->objToArray($response);
    foreach ($responseArray as $key => $value) {
      $slipArray[$value['slipNo']][] = $value;
    }
    return $slipArray;
  }


  public function bagExists()
  {
    $response = $this->noOrderArray();
    $nobagArrays = array();
    $result = array();
    $hasBaggage = array();

    foreach ($response as $key => $value) {
      foreach ($value as $nkey => $nvalue) {
        if ($nvalue['code'] === 200) {
          $hasBaggage[$key][] = $nvalue;
        }
        //エラーコード処理
        if ($nvalue['code'] === 404) {
          $noBag[$key][] = $nvalue;
        }else if($nvalue['code'] === 400){
          $noBag[$key][] = $nvalue;
        }else if($nvalue['code'] === 401){
          $noBag[$key][] = $nvalue;
        }else if($nvalue['code'] === 500){
          $noBag[$key][] = $nvalue;
        }
      }
    }


    foreach ($noBag as $key => $value) {

      if (count($value) >= count($this->company_name)) {
        $nobagArrays[$key]="";
        $nobagArrays[$key] = array(
            'code'=>$value[0]['code'],
            'status'=>$value[0]['status'],
            'slipNo'=>$key,
            );
      }
    }

    if(!empty($hasBaggage)){
      foreach ($hasBaggage as $key => $value) {
        foreach ($value as $key => $ivalue) {
          $result[$value[0]['slipNo']] = $ivalue;
        }
      }
    }
    foreach ($nobagArrays as $key => $value) {
      $result[$value['slipNo']] = $value;
    }
    return $this->result = $result;
  }


  public function searchAll()
  {
    $this->bagExists();
  }


  public static function errorJson($error)
  {
    header('content-type: application/json; charset=utf-8');
    header('Access-Control-Allow-Origin: *');
    return $error;
  }

  public function getSearchResult()
  {
    return(json_encode(array_values($this->result),JSON_UNESCAPED_UNICODE));
  }


}
