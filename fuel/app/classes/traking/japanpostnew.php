<?php

class Traking_Japanpostnew
{
  private $number;
  private $history_status;
  private $xml;
  private $result;

  public function __construct($number,$html)
  {
    $this->number = $number;
    $this->html   = $html;
    $this->history_status = '//*[@id="content"]/form/div/table[2]';

    //チェックディジット
    $traking_number = Traking_Common::checkdigit($this->number);
    $this->number = $traking_number['traking_number'];

  }

  public function tracking_history()
  {
    $dom = $this-> html_to_dom();
    $xml = $this->get_xml();
    $xpath = new DOMXpath($dom);

    if ($xpath->query($this->get_history_status())->length === 0) {
      throw new Exception('unregistered');//商品が取得出来なかった場合
    }


    foreach ($xpath->query($this->get_history_status()) as $node) {
      $node;
    }


    $node_array = explode("\n", $node->textContent);

    foreach ($node_array as $key => $value) {
      $nospace_array[] = trim($value);
    }


    foreach ($nospace_array as $key => $value) {
      if ($key <= 12) {
        unset($nospace_array[$key]);
      }
    }
    $status_array = array_values($nospace_array);

    //日付時刻データの分割
    foreach ($status_array as $key => $value) {
      if ($key % 6 === 0) {
        if (strlen($value) === 10) {//時刻データがない場合
          $date[] = array('date' => substr($value, -5));
          $time[] = array('time' => "");
        }else{//時刻データがある場合
          $date[] = array('date' => substr($value, -10, -5));
          $time[] = array('time' => substr($value, -5));
        }
      }
    }

    $status_arrays = array_chunk($status_array, 6);
    array_pop($status_arrays);

    $keys = array(
      'date',
      'time',
      'date_time',
      'status',
      'space',
      'placeName',
      'address',
      'placeCode',
    );

    foreach ($status_arrays as $key => $value) {
      foreach ($time as $keyd => $valued) {
        array_unshift($status_arrays[$key],$valued['time']);
        unset($time[$key]);
        break;
      }
      foreach ($date as $keyd => $valued) {
        array_unshift($status_arrays[$key],$valued['date']);
        unset($date[$key]);
        break;
      }

    }

    foreach ($status_arrays as $key => $value) {
      $status[] = array_combine($keys,$status_arrays[$key]);
    }

    foreach ($status as $key => $value) {
      unset($status[$key]['date_time'],$status[$key]['space'],$status[$key]['address']);
    }

    $this->result = $status;
  }


  public function get_result(){
    return $this->result;
  }


  private function html_to_dom()
  {
    $dom = new DOMDocument();
    @$dom->loadHTML($this->get_html());
    $this->xml = simplexml_import_dom($dom);
    return $dom;
  }


  private function get_html()
  {
    return $this->html;
  }

  private function get_history_status()
  {
    return $this->history_status;
  }
  private function get_xml()
  {
    return $this->xml;
  }


}
