<?php
use \Model\Slipnumbers;

class Traking_Common
{
  public $number;
  public $errors;
  public $errorMessage;
  public $company_names;

  public function __construct($number)
  {
    $this->number = $number;
    $this->errors['bool'] = 0;
  }

  public function validation()
  {
    if (mb_strlen($this->number) === 0) {
      throw new Exception('unentered');//番号未入力
    }
    if (mb_strlen($this->number) != 12) {
      throw new Exception('number_error');//12ではなかった場合
    }
  }

  public static function checkdigit($number)
  {
    $str_len12 = strlen($number);
    $checkn = substr($number,0,11);
    $snumber = substr($number,0,-1);
    $fnumber = $snumber.($checkn % 7);
    if ($str_len12 === 12 && $number === $fnumber) {

      return $result = array('boolean'=>true,'traking_number'=>$fnumber);
    }else{
      throw new Exception('checkdigit_error');
    }
  }

  public static function fileget($number)
  {
    try{
      $company_name = Uri::segments();

      //ユーザエージェントの変更とファイルの取得
      $options = array(
        'http' => array(
          'method' => 'GET',
          'header'=>"User-Agent: nimotsu_trackerbot (tsuyokashi@gmail.com)",
        ),
      );
      $context = stream_context_create($options);
      //佐川運輸
      if ($company_name[1] === 'sagawa') {
        $url = "https://k2k.sagawa-exp.co.jp/p/web/okurijosearch.do?okurijoNo=".$number;
        $http_response_header = array();

        //ヤマト運輸
      }elseif($company_name[1] === 'yamato'){
        $url = 'https://toi.kuronekoyamato.co.jp/cgi-bin/tneko';
        $http_response_header = array();

        $data["number00"] = 1;//詳細情報表示フラグ
        $data["number01"] = $number;
        $data = http_build_query($data, "", "&");
        $options = array('http' => array(
          'method' => 'POST',
          'content' => $data,
        ));
        $context = stream_context_create($options);

        //日本郵便　
      }elseif ($company_name[1] === 'japanpost') {
        $options = array(
          'http' => array(
            'method' => 'GET',
            'header'=>"User-Agent: Mozilla/5.0 (Windows NT 5.1; rv:13.0) Gecko/20100101 Firefox/13.0.1",
          ),
        );
        $context = stream_context_create($options);
        $url="https://trackings.post.japanpost.jp/services/srv/search/?requestNo1=$number"."&search=%E8%BF%BD%E8%B7%A1%E3%82%B9%E3%82%BF%E3%83%BC%E3%83%88";
      }

      Log::debug($url);

      if ($html = @file_get_contents($url, false, $context)) {
        return $html;
      }else{
        //エラー処理
        if(count($http_response_header) > 0){
          $status_code = explode(' ', $http_response_header[0]);

          switch($status_code[1]){
            case 404:
            throw new Exception('指定したページが見つかりませんでした');
            break;

            case 500:
            throw new Exception('指定したページがあるサーバーにエラーがあります');
            break;

            default:
            throw new Exception('何らかのエラーによって指定したページのデータを取得できませんでした');
          }
        }else{
          throw new Exception("タイムエラー or URLが間違っています");
        }
      }

    }catch(Exception $e){
      header('content-type: application/json; charset=utf-8');
      header('Access-Control-Allow-Origin: *');
      echo $errorJson = '{"code": "500","status": "'.$e->getMessage().'","number": "'.$number.'"}';
      exit(1);
    }

  }

  public function outJson($status_data)
  {
    sleep(0.8);//スクレイピングの遅延
    header('content-type: application/json; charset=utf-8');
    header('Access-Control-Allow-Origin: *');
    $company_name = Uri::segments();

    if ($this->errors['bool'] === 0) {
      $last = end($status_data);
      $last_status = array();
      $last_status['code'] = (int)"200";
      $last_status['companyName'] = $company_name[1];
      $last_status['status'] = $last['status'];

      $last_status['slipNo'] = (int)$this->number;
      $last_status['statusList'] = $status_data;

      $json = json_encode($last_status,JSON_UNESCAPED_UNICODE);
      $json = str_replace('↓','',$json);
      $json = str_replace('⇒','',$json);
      
      Slipnumbers::in_numbers($this->number);

      return $json;

    }
    elseif($this -> errors['bool'] === 1)
    {

      //エラー処理

      if (empty($this->errors['error_message']) || empty($this->errors['error_code']))
      {
        $this->errors['error_code'] = 501;
        $this->errors['error_message'] = '想定外のエラー';
        return $errorJson = '{"code":'.(int)$this->errors['error_code'].',"companyName":"'.$this->errors['company_name'].'","status": "'.$this->errors['error_message'].'","slipNo": '.(int)$this->number.'}';
      }
      else
      {
        return $errorJson = '{"code":'.(int)$this->errors['error_code'].',"companyName":"'.$this->errors['company_name'].'","status": "'.$this->errors['error_message'].'","slipNo": '.(int)$this->number.'}';
      }

    }
    else
    {
      return $errorJson = '{"code": 500,"status": "その他のエラー","slipNo": "'.(int)$this->number.'"}';
    }

  }


  public function exception_messsage($errorMessage)
  {
    $this->errorMessage = $errorMessage;
    $company_name = Uri::segment(2);

    $this->errors['bool'] = 1;
    $this->error['error_code'] ="";

    switch ($this->errorMessage) {

      case 'number_error':
      $this->errors['error_code'] = 401;
      $this->errors['error_message'] = '桁数エラー';
      $this->errors['company_name'] = $company_name;
      break;

      case 'unregistered':
      $this->errors['error_code'] = 404;
      $this->errors['error_message'] = "荷物データが登録されておりません";
      $this->errors['company_name'] = $company_name;
      break;

      case 'unentered':
      $this->errors['error_code'] = 403;
      $this->errors['error_message'] = "番号が入力されていません";
      $this->errors['company_name'] = $company_name;
      break;

      case 'checkdigit_error':
      $this->errors['error_code'] = 400;
      $this->errors['error_message'] = 'チェックディジットエラー';
      $this->errors['company_name'] = $company_name;
      break;

      case'count_error':
      $his->errors['error_code'] = 501;
      $this->errors['error_message'] = 'カウントエラー';
      $this->errors['company_name'] = $company_name;
      break;

      default:
      $this->errors['error_code'] = 500;
      $this->errors['error_message'] = 'その他のエラー';
      $this->errors['company_name'] = $company_name;
      break;
    }
  }




}
