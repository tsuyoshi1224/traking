<?php

class Traking_Yamato
{
  private $number;
  public $html;
  public $contents;
  public $itemtype;
  public $detail;

  public function __construct($number,$html)
  {
    $this->number =   $number;
    $this->html =     $html;
    $this->html = mb_convert_encoding($this->html,'utf8','SJIS-win');
    $this->contents = str_replace(array("\r", "\n"), '', $this->html);

    //バリデーション
    $vali = new Traking_Common($this->number);
    $vali -> validation();

    //チェックディジット
    $traking_number = Traking_Common::checkdigit($this->number);
    $this->number = $traking_number['traking_number'];

    return $this->number;
  }

  //商品名の取得
  public function product_name_get()
  {
    preg_match('/<td.nowrap>*.*<\/td>/iu', $this->contents, $itemtypeArray);
    $this->itemtype = $itemtypeArray;

    if (empty($this->itemtype)) {
      throw new Exception('unregistered');//商品が取得出来なかった場合
    }

    //商品名の取得
    preg_match('/(<[^>]+>[^<]+<\/[^>]+>|[^<]+)/', $this->itemtype[0], $this -> itemtype);
    $this->itemtype = preg_replace('/td.nowrap>/','',$this->itemtype[0]);//HTMLタグの削除

    return $this->itemtype;
  }

  public function meisai_tag()
  {
    preg_match_all('/<table.class="meisai".*?>(.*?)<\/table>/iu', $this->contents, $this->detail); //<table class"meisai">の抽出
    //HTMLタグを基準に配列へ
    foreach($this->detail[0] as $str) {
      preg_match_all('/<td.*?>(.*?)<\/td>/iu',$str,$baggage_datas);//荷物テーブルのの抽出
      $this->detail=preg_replace('/<("[^"]*"|\'[^\']*\'|[^\'">])*>/','',$baggage_datas[0]);//HTMLタグの削除
    }
    return $this->detail;
  }

  public function statusBuilding()
  {
    $baggage=(array_chunk($this->detail,6,true));//配列を６つに分割
    $keys = array(array("space","status","date","time","placeName","placeCode"));

    //キーと値の結合
    $i = 0;
    $result = array();
    foreach($baggage as $value){
      $status_array=(array_combine($keys[0], $baggage[$i]));
      $i++;
      unset($status_array["space"]);
      array_push($result, $status_array);
    }
    return $result;
  }



}
