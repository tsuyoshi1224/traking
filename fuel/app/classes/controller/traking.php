<?php

class Controller_Traking extends Controller_Rest
{
  public function action_index()
  {
    return Response::forge(View::forge('traking/index'));
  }

public function action_japanpost()
{
  try {
    $number = Input::get('number');
    $number = preg_replace('/[^0-9\.]/', '', $number);

    $html = Traking_Common::fileget($number);

    $japanpost = new Traking_Japanpostnew($number,$html);
    $japanpost -> tracking_history();


    $status = new Traking_Common($number);
    $this->response($status->outJson($japanpost->get_result()));
    // $this->response($japanpost->get_result());
    
  } catch (\Exception $e) {
    $errorMessage = $e->getMessage();

    $status = new Traking_Common($number);
    $status -> exception_messsage($errorMessage);
    $this->response($status->outJson(""));
  }




}

//ヤマト
  public function action_yamato()
  {
    try {
      $number = Input::get('number');
      $number = preg_replace('/[^0-9\.]/', '', $number);

      $html = Traking_Common::fileget($number);
      $yamato = new Traking_Yamatonew($number,$html);
      $yamato -> tracking_history();
      $results = $yamato->get_result();

      $status = new Traking_Common($number);
      echo $status -> outJson($results);
    } catch (Exception $e) {
      $errorMessage = $e->getMessage();
      $status = new Traking_Common($number);
      $status -> exception_messsage($errorMessage);
      $this->response($status->outJson(""));
    }
  }



//佐川
  public function action_sagawa()
  {
    try{
      $number = Input::get('number');
      $number = preg_replace('/[^0-9\.]/', '', $number);
      $statuses = new Traking_sagawa();
      $statuses->statusGet($number);
      $statuses->statusBuilding();
      $status_data = $statuses->status;
      $status = new Traking_Common($number);
      // $this->response($status)
      echo $status -> outJson($status_data);

    }catch(Exception $e){
      $errorMessage = $e->getMessage();
      $status = new Traking_Common($number);
      $status -> exception_messsage($errorMessage);
      $this->response($status->outJson(""));
    }

    // return Response::forge(View::forge('sagawa/api'));
  }


// すべて検索ページ
  public function action_allsearch()
  {
    try {
      sleep(1);
      $raw_post = file_get_contents('php://input');
      $allSearch = new Traking_Allsearch($raw_post);
      $allSearch -> searchAll();
      // header('Access-Control-Allow-Origin: *');
      // header('content-type: application/json; charset=utf-8');
      // echo $allSearch -> getSearchResult();
      $this->response($allSearch->getSearchResult());
    } catch (\Exception $e) {
      $this->response(Tracking_allsearch::errorJson($e->getMessage()));
    // echo json_encode(Traking_Allsearch::errorJSON($e->getMessage()));

    }

  }


}
