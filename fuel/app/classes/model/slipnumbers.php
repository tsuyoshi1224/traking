<?php
namespace Model;

class Slipnumbers extends \Model {

  public static function get_numbers()
  {
    try {

      return \DB::select()
      ->from('slip_numbers')
      ->execute()
      ->as_array();

    } catch (\Exception $e) {
      \Log::error($e);
    }
  }

  public static function in_numbers($number)
  {
    try {

      if (!gettype($number) === 'integer') {
        throw new Exception('int型以外の挿入');
      }
      
      $result = \DB::insert('slip_numbers')->set(array(
        'slip_number' => $number,
      ) )->execute();

      if (!$result[1]>0) {
        throw new Exception('DB挿入失敗');
      }

    } catch (\Exception $e) {
      \Log::error($e);
    }

  }
}
