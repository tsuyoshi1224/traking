<?php
return array(
	'_root_'  => 'traking/index',  // The default route
	'_404_'   => 'error/index',    // The main 404 route

	'hello(/:name)?' => array('welcome/hello', 'name' => 'hello'),
);
