<!DOCTYPE html>
<html lang="ja">
<head>
  <!-- Google Analytics -->
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-100109386-1', 'auto');
  ga('send', 'pageview');
  </script>
  <!-- End Google Analytics -->

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-Script-Type" content="text/javascript">
	<meta name="description" content="荷物トラッカーは大手三社の荷物番号を追跡することができます。">
  <title>宅配トラッカーベータ版</title>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://bootswatch.com/3/sandstone/bootstrap.min.css">
  <?= Asset::css('top.css'); ?>
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
  <nav class="navbar navbar-default">
    <a class="navbar-brand" href="#">荷物トラッカーβ(ベータ)版</a>
    <div class="collapse navbar-collapse">
    </div>
  </nav>
  <div class="container">
    <div class="row">
      <?= Asset::img('img04.png', array('class' => 'img-responsive center-block')); ?>
      <div class="col-xs-12">
        <label for="">伝票番号</label>
        <form class="form-horizontal" method="POST">
          <div class="form-group">
            <div class="center-block">
              <textarea placeholder="各行に１つの伝票番号を入力します" rows="3" class="form-control" id="slipNo"></textarea>
            </div>
          </div>
          <input class="btn btn-default center-block" type="button" value="送信" onclick="ajaxSearch();">
        </form>
      </div>





      <div class="tabs">
        <div class="panel panel-default">
          <div class="panel-body">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#list" data-toggle="tab">一覧</a></li>
              <li><a href="#csv" data-toggle="tab">csv</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane  active" id="list">
                <table id="list-result"class="table">
                  <thead>
                    <tr>
                      <th>会社名</th>
                      <th>荷物番号</th>
                      <th>最終ステータス</th>
                      <th>日付</th>
                      <th>時刻</th>
                      <th>営業所</th>
                      <th>詳細</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>

              </div>
              <div class="tab-pane" id="csv">
                <div class="col-xs-12">
                  CSV出力
                </div>
                <div class="col-xs-12">
                  <textarea id="resultTextArea" rows="10" class="form-control"></textarea>
                </div>
                <div class="col-xs-12">
                  <button class="btn btn-default copyBtn" type="button" name="textAreaButton" onclick="execCopy(this)">コピーボタン</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>


    </div>

  </div>


  <!-- モーダル・ダイアログ -->
  <div class="modal fade" id="detailModal" tabindex="-1">
  	<div class="modal-dialog">
  		<div class="modal-content">
  			<div class="modal-header">
  				<button type="button" class="close" data-dismiss="modal"><span>×</span></button>
  				<h4 class="modal-title">タイトル</h4>
  			</div>
  			<div class="modal-body">
  				<table class="table">
            <thead>
              <tr>
                <th>ステータス</th>
                <th>日付</th>
                <th>時刻</th>
                <th>営業所名</th>
                <th>営業所番号</th>
              </tr>
            </thead>
            <tbody>
            </tdoby>
          </table>
  			</div>
  			<div class="modal-footer">
  				<button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
  			</div>
  		</div>
  	</div>
  </div>



  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  <?= Asset::js('functions.js'); ?>
  <?= Asset::js('modal.js'); ?>
  <?= Asset::js('ajax.js'); ?>
</body>
</html>
